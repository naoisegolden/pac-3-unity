using System;
using UnityEngine;

public class CollectibleController : MonoBehaviour
{
	[SerializeField] private Animator animator;
	[SerializeField] private new AudioSource audio;

	public Action OnCollected;

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Player"))
		{
			OnCollected?.Invoke();

			animator.SetBool("IsCollected", true);
			audio.Play();
		}
	}

	private void OnCollectedAnimationEnd()
	{
		Destroy(gameObject);
	}
}
