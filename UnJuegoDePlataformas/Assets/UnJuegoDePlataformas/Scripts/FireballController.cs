using UnityEngine;

public class FireballController : MonoBehaviour
{
	[SerializeField] private float speed = 10f;
	[SerializeField] private Rigidbody2D rb;
	[SerializeField] private GameObject impactEffect;

	void Start()
	{
		rb.velocity = transform.right * speed;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		Destroy(gameObject);

		// Show impact effect and destroy when animation finished
		GameObject effect = Instantiate(impactEffect, transform.position, transform.rotation);
		Destroy(effect, GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
	}
}
