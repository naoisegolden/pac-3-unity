using System;
using UnityEngine;

public class LootboxController : CollidableBoxController
{
	[SerializeField] private Animator animator;
	[SerializeField] private new AudioSource audio;

	public Action OnOpened;

	private bool isEmpty = false;

	public override void OnCollision()
	{
		if (!isEmpty)
		{
			animator.SetBool("IsEmpty", true);
			isEmpty = true;
			animator.Play("Base Layer.Lootbox_Open");
			audio.Play();

			OnOpened?.Invoke();
		}
	}
}