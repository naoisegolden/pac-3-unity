using System;
using UnityEngine;

public class OpenDoorController : MonoBehaviour
{
	public Action OnEnter;

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Player"))
		{
			OnEnter?.Invoke();
			OnEnter = null; // So that is only called once
		}
	}
}
