using UnityEngine;

public class EnemyController : MonoBehaviour
{
	[SerializeField] private float speed = 1f;
	[SerializeField] private Animator animator;
	[SerializeField] private AudioSource audioDead;

	private Rigidbody2D rb;
	private float horizontalDirection;
	private bool isDead = false;
	void Awake()
	{
		rb = GetComponent<Rigidbody2D>();
		horizontalDirection = Mathf.Sign(transform.localScale.x);
	}

	private void Update()
	{
		Move();
	}

	void Move()
	{
		float moveBy = horizontalDirection * speed;
		rb.velocity = new Vector2(moveBy, rb.velocity.y);
	}

	private void Flip()
	{
		horizontalDirection *= -1;

		// Flip game object in x
		transform.Rotate(0f, 180f, 0f);
	}

	private void Die()
	{
		isDead = true;
		animator.SetBool("IsDead", true);
		speed = 0;
		audioDead.Play();
		Destroy(gameObject, 1f);
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Killer"))
		{
			Die();
		}

	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		// Detect direction of contact
		ContactPoint2D contact = other.GetContact(0);
		float collisionDirectionX = contact.normal.x > 0 ? -1 : 1;
		bool collisionFromTop = contact.normal.y < -0.5;

		// Flip when colliding on x
		if (Mathf.Sign(horizontalDirection * collisionDirectionX) > 0)
		{
			Flip();
		}

		if (!isDead && other.gameObject.CompareTag("Player"))
		{
			if (collisionFromTop)
			{
				Die();
			}
			else
			{
				other.gameObject.GetComponent<PlayerController>().Hit();
			}
		}
	}
}
