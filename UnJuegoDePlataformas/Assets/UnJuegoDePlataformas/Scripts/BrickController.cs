using UnityEngine;

public class BrickController : CollidableBoxController
{
	[SerializeField] private new AudioSource audio;

	private ParticleSystem particles;
	private SpriteRenderer sprite;
	private new BoxCollider2D collider;

	private void Start()
	{
		sprite = GetComponent<SpriteRenderer>();
		collider = GetComponent<BoxCollider2D>();
		particles = GetComponent<ParticleSystem>();
	}

	public override void OnCollision()
	{
		sprite.enabled = false;
		collider.enabled = false;
		if (audio) { audio.Play(); }
		if (particles) { particles.Play(); }
	}

	public void OnParticleSystemStopped()
	{
		gameObject.SetActive(false);
	}
}
