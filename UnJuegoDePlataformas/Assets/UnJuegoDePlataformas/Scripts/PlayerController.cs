using System;
using System.Collections;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
	public Action OnKill;
	public Action OnHit;
	[HideInInspector] public int lives = 3;

	private Rigidbody2D rb;
	private bool isGrounded = false;
	private bool isFacingRight = true;
	private bool canMove = true;
	private bool canBeHit = true;
	private bool hasSuperpower = false;
	private float horizontalMove;

	[SerializeField] private float speed = 5f;
	[SerializeField] private float jumpPower = 5f;
	[SerializeField] private float hitPower = 2.5f;
	[SerializeField] private Animator animator;
	[SerializeField] private AudioSource audioJump;
	[SerializeField] private AudioSource audioHit;
	[SerializeField] private AudioSource audioMove;

	private void Awake()
	{
		rb = GetComponent<Rigidbody2D>();
	}

	private void Start()
	{
		StartCoroutine(Footsteps());
	}

	void Update()
	{
		if (canMove)
		{
			Move();
		}
		if (Input.GetButtonDown("Jump") && isGrounded == true && canMove)
		{
			Jump();
		}
		if (Input.GetButtonDown("Fire1") && hasSuperpower)
		{
			Shoot();
		}
	}
	void Move()
	{
		horizontalMove = Input.GetAxisRaw("Horizontal");
		float moveBy = horizontalMove * speed;
		rb.velocity = new Vector2(moveBy, rb.velocity.y);

		animator.SetFloat("Speed", Mathf.Abs(moveBy));

		if (moveBy > 0 && !isFacingRight)
		{
			Flip();
		}
		else if (moveBy < 0 && isFacingRight)
		{
			Flip();
		}
	}

	void Jump()
	{
		rb.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
		isGrounded = false;
		animator.SetBool("IsJumping", true);
		audioJump.Play();
	}

	private void Flip()
	{
		isFacingRight = !isFacingRight;

		// Flip game object in x
		transform.Rotate(0f, 180f, 0f);
	}

	private void Shoot()
	{
		StartCoroutine(nameof(AnimateShoot));
		GetComponent<WeaponController>().Shoot();
	}

	public void Hit()
	{
		if (!canBeHit) return;

		// Remove a life
		lives -= 1;
		if (lives <= 0)
		{
			OnKill?.Invoke();
		}

		// Animate
		StartCoroutine(nameof(AnimateHit));

		audioHit.Play();

		OnHit?.Invoke();

		DisableSuperpower();

	}

	private void Freeze()
	{
		rb.constraints = RigidbodyConstraints2D.FreezePosition;
	}

	private void StopMoving()
	{
		rb.velocity = new Vector2(0, rb.velocity.y);
		canMove = false;
	}
	private void ResumeMoving()
	{
		canMove = true;
	}

	public void EnableSuperpower()
	{
		hasSuperpower = true;
		animator.SetBool("HasSuperpower", true);
	}

	public void DisableSuperpower()
	{
		hasSuperpower = false;
		animator.SetBool("HasSuperpower", false);
	}

	private IEnumerator AnimateHit()
	{
		const float AnimationDelay = .5f;

		canBeHit = false;
		animator.SetBool("IsHit", true);

		// Jump backwards
		StopMoving();
		rb.AddForce(Vector2.up * hitPower, ForceMode2D.Impulse);
		rb.AddForce(new Vector2(-horizontalMove * hitPower, 0f), ForceMode2D.Impulse);

		yield return new WaitForSeconds(AnimationDelay);

		canBeHit = true;
		animator.SetBool("IsHit", false);
		ResumeMoving();
	}

	private IEnumerator AnimateShoot()
	{
		animator.SetBool("IsFiring", true);

		yield return new WaitForSeconds(0.2f);

		animator.SetBool("IsFiring", false);
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		// End jump
		isGrounded = true;
		animator.SetBool("IsJumping", false);
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Killer"))
		{
			lives = 0;
			OnKill?.Invoke();
		}

	}

	private IEnumerator Footsteps()
	{
		if (horizontalMove != 0 && isGrounded)
		{
			audioMove.Play();
		}
		yield return new WaitForSeconds(.2f);
		StartCoroutine(Footsteps());
	}
}
