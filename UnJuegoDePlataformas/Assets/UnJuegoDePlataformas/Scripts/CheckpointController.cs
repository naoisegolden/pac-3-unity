using System;
using UnityEngine;

public class CheckpointController : MonoBehaviour
{
	[SerializeField] private Animator animator;
	[SerializeField] private new AudioSource audio;

	public Action<Transform> OnChecked;

	private bool isChecked = false;

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Player") && !isChecked)
		{
			animator?.SetBool("IsChecked", true);
			isChecked = true;
			audio.Play();

			OnChecked?.Invoke(gameObject.transform);
		}

	}
}
