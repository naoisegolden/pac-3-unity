using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

enum GameState
{
	Playing,
	Paused,
	Won,
	Lost
}

struct GameData
{
	// Parameterless constructor are only available in C# 10 and up,
	// so I added a bogus parameter.
	// See https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/struct#limitations-with-the-design-of-a-structure-type
	public GameData(string someValue)
	{
		state = GameState.Playing;
		startTime = 0f;
		isPaused = false;
		_playerStartX = 0f;
		coinsCount = 0;
	}
	public GameState state;
	public bool isPaused;
	private float _playerStartX;
	public float playerStartX
	{
		set
		{
			// To persist when reloading scene
			PlayerPrefs.SetFloat("playerStartX", value);
			_playerStartX = value;
		}
		get { return _playerStartX; }
	}
	public int coinsCount;
	public float startTime;
}

public class GameplayManager : MonoBehaviour
{
	[Header("Game options")]
	[SerializeField] private int maxLives = 3;

	[Header("Configuration")]
	[SerializeField] private PlayerController player;
	[SerializeField] private SwitchController doorSwitch;
	[SerializeField] private GameObject closedDoor;
	[SerializeField] private GameObject openDoor;
	[SerializeField] private GameObject canvas;
	[SerializeField] private GameObject uiCoinsCount;
	[SerializeField] private GameObject uiStatsCoinsCount;
	[SerializeField] private GameObject uiStatsLivesCount;
	[SerializeField] private GameObject uiStatsTimeCount;
	[SerializeField] private GameObject pauseOverlay;
	[SerializeField] private GameObject lostOverlay;
	[SerializeField] private GameObject wonOverlay;
	[SerializeField] private AudioSource audioLost;
	[SerializeField] private AudioSource audioWon;
	[SerializeField] private AudioSource audioUI;

	// Game Data
	private GameData game = new GameData("unused");

	// Dynamic UI
	private List<GameObject> livesUI = new List<GameObject>();

	void Awake()
	{
		// Populate actions
		player.OnKill += KillPlayer;
		player.OnHit += HitPlayer;
		doorSwitch.OnPressed += OpenDoor;
		openDoor.GetComponent<OpenDoorController>().OnEnter += WinLevel;

		// Populate actions for collectibles
		GameObject[] coins = GameObject.FindGameObjectsWithTag("Coin");
		foreach (GameObject coin in coins)
		{
			coin.GetComponent<CoinController>().OnCollected += AddCoin;
		}
		GameObject[] stars = GameObject.FindGameObjectsWithTag("Star");
		foreach (GameObject star in stars)
		{
			Debug.Log("star found");
			star.GetComponent<StarController>().OnCollected += CollectStar;
		}

		// Populate actions for checkpoints
		GameObject[] checkpoints = GameObject.FindGameObjectsWithTag("Checkpoint");
		foreach (GameObject checkpoint in checkpoints)
		{
			checkpoint.GetComponent<CheckpointController>().OnChecked += SaveCheckpoint; ;
		}
	}

	void Start()
	{
		// Draw dynamic UI
		InstantiateUILives();
		// Put player in the right checkpoint
		InstantiatePlayer();

		StatePlaying();
	}

	private void Update()
	{
		if (Input.GetKeyDown("escape"))
		{
			if (game.state == GameState.Playing)
			{
				StatePause();
				audioUI.Play();
			}
			else if (game.state == GameState.Paused)
			{
				StatePlaying();
				audioUI.Play();
			}
			else if (game.state == GameState.Lost)
			{
				RestartLevel();
				audioUI.Play();
			}
			else if (game.state == GameState.Won)
			{
				EndGame();
			}

		}

	}

	private void InstantiatePlayer()
	{
		float playerStartX = PlayerPrefs.GetFloat("playerStartX");

		player.lives = maxLives;
		Vector2 playerCheckpointTranslate = new Vector2(0.7f, -0.7f);
		player.transform.position = new Vector2(playerStartX, 0) + playerCheckpointTranslate;
	}

	private void StatePlaying()
	{
		Pause(false);
		game.state = GameState.Playing;
		UpdateUI();
	}

	private void StatePause()
	{
		Pause(true);
		game.state = GameState.Paused;
		UpdateUI();
	}

	private void StateLost()
	{
		Pause(true);
		game.state = GameState.Lost;
		audioLost.Play();
		UpdateUI();
	}

	private void StateWon()
	{
		Pause(true);
		game.state = GameState.Won;
		audioWon.Play();
		UpdateUI();
	}

	private void Pause(bool pause)
	{
		Time.timeScale = pause ? 0f : 1f;
		game.isPaused = pause;
	}

	private void RestartLevel()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	private void EndGame()
	{
		SceneManager.LoadScene("Start", LoadSceneMode.Single);
	}

	private void WinLevel()
	{
		StateWon();
	}

	private void OpenDoor()
	{
		closedDoor.SetActive(false);
		openDoor.SetActive(true);
	}

	private void AddCoin()
	{
		Debug.Log("add coin");
		game.coinsCount += 1;
		UpdateUI();
	}

	private void CollectStar()
	{
		Debug.Log("star collected");
		player.EnableSuperpower();
	}

	private void HitPlayer()
	{
		UpdateUI();
	}

	private void KillPlayer()
	{
		Invoke(nameof(StateLost), .1f);
	}

	private void SaveCheckpoint(Transform checkpoint)
	{
		game.playerStartX = checkpoint.position.x;
	}

	private void UpdateUI()
	{
		// Draw coins
		uiCoinsCount.GetComponent<Text>().text = $"{game.coinsCount}";

		// Draw lives
		int i = 0;
		Sprite sprite;
		foreach (GameObject life in livesUI)
		{
			if (i < player.lives)
			{
				sprite = Resources.Load<Sprite>("Sprites/hudHeart_full");
			}
			else
			{
				sprite = Resources.Load<Sprite>("Sprites/hudHeart_empty");

			}
			life.GetComponent<Image>().sprite = sprite;
			i++;
		}

		// Draw stats
		float timer = Time.time - game.startTime;
		float minutes = Mathf.Floor(timer / 60);
		float seconds = Mathf.RoundToInt(timer % 60);
		uiStatsCoinsCount.GetComponent<Text>().text = $"{game.coinsCount}";
		uiStatsLivesCount.GetComponent<Text>().text = $"{player.lives}";
		uiStatsTimeCount.GetComponent<Text>().text = $"{minutes} min {seconds} sec";

		// Show overlays
		pauseOverlay.SetActive(game.state == GameState.Paused);
		lostOverlay.SetActive(game.state == GameState.Lost);
		wonOverlay.SetActive(game.state == GameState.Won);
	}

	private void InstantiateUILives()
	{
		const float y = 190f;
		const float x = 360f;
		const float scale = .4f;
		const float distance = 35f;

		for (int i = 0; i < maxLives; i++)
		{
			// Generate UI image in canvas
			GameObject obj = new GameObject($"Life ({i})");
			obj.AddComponent<Image>();
			obj.transform.SetParent(canvas.transform);

			// Move to position
			obj.GetComponent<RectTransform>().localPosition = new Vector2(x - i * distance, y);
			obj.transform.localScale = new Vector2(scale, scale);

			livesUI.Add(obj);
		}
	}
}
