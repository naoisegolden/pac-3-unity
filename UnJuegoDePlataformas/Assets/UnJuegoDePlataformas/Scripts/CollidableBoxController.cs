using UnityEngine;

public class CollidableBoxController : MonoBehaviour
{
	public virtual void OnCollision()
	{
		// Override this method
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			// Detect direction of contact in y
			ContactPoint2D contact = other.GetContact(0);
			float collisionDirection = contact.normal.y > 0 ? -1 : 1;

			if (collisionDirection == -1)
			{
				OnCollision();
			}

		}
	}
}
