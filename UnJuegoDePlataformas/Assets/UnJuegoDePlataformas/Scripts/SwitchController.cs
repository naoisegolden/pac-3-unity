using System;
using UnityEngine;

public class SwitchController : MonoBehaviour
{
	[SerializeField] private Animator animator;
	[SerializeField] private new AudioSource audio;

	public Action OnPressed;

	private bool isPressed = false;

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Player") && !isPressed)
		{
			OnPressed?.Invoke();
			OnPressed = null; // So that is only called once

			isPressed = true;

			animator.SetBool("IsPressed", true);
			audio.Play();
		}
	}
}
