using UnityEngine;
using UnityEngine.SceneManagement;

public class StartSceneManager : MonoBehaviour
{
	[SerializeField] private AudioSource audioUI;

	private void Start()
	{
		PlayerPrefs.DeleteAll();
	}

	void Update()
	{
		if (Input.anyKey)
		{
			LoadGame();
		}
	}

	private void LoadGame()
	{
		SceneManager.LoadScene("Level1");
	}
}
