using UnityEngine;

public class WeaponController : MonoBehaviour
{
	[SerializeField] private Transform firePoint;
	[SerializeField] private GameObject fireballPrefab;
	[SerializeField] private AudioSource audioFire;

	public void Shoot()
	{
		audioFire.Play();
		Instantiate(fireballPrefab, firePoint.position, firePoint.rotation);
	}
}
