# PAC 3 - Un joc d'artilleria

Video: https://youtu.be/U7mwFwKnopY

I decided to continue with the same game done in PAC2, adding the described points for PAC3:

### 1. At least one character with two or more animations

Player has animations for idle, walking, jumping, firing and damage.

Enemies have animations for walking and dead.

Collectibles have animations for idle and collected.

Lootboxes have animations for unopened, opening and empty.

Checkpoints have animations for unchecked and checked.

Fireballs have animations for being fired and exploding.

Other animations that are not using the state machine are the particles in the lava pits and the particles of the broken bricks.

### 2. At least one particle system

Added a particle system to the lava pits, to add an effect to convey the danger (heat).

Added a particle system with gravity and burst emission to animate when bricks are broken. Uses stop action callback to disable the game object. 

### 3. Enemies with AI

Partially done: they change direction on colliding, with invisible colliders only for enemies. They also change direction when colliding between each other and with the player. 

### 4. Be able to shoot at enemies

The `Fire1` button, which is mapped to `ctrl`, throws a fireball that kills enemies. This fireball has an end animation for when it collides. 

The logic of being killed by the fireball is in the receiver (the Enemy), so that the fireball doesn't need to know how it affects different characters.

Also, as mentioned in Architecture, it's done in a way that can be scaled to different weapons and ammunition.

### 5. Game state control: revive character

I use a struct `GameData` to encapsulate all game data, which contains the Player's starting x position in `playerStartX`. 

Since the instance of the struct is destroyed and recreated on `LoadScene`, I overwrite the `set` of the initial player x position so that it also saves it in the `PlayerPrefs`. When the player triggers a checkpoint, the `x` position of the checkpoint is saved in the `GameData` and also in the `PlayerPrefs`; allowing reloading the whole scene form scratch but persisting this value. This is not from any tutorial, so I am not sure if it's the right way to do it, but it works and I think it's elegant and robust enough.

### 6. Use Tags and/or Layers

Using tags for `Enemy`, `Coin`, `Star`, `Killer` and `Checkpoint`, to identify collisions and triggers. Using these tags, the Gameplay Manager can also programmatically populate logic in these game objects.

Using layer `Cam Boundary` to inform Cinemachine where the end of the scene is.
Using layers `Enemies` and `Enemy Walls` for invisible collisions exclusive for enemies, so that they patrol.

### TO DO's: 

- Finish lootbox logic
- Animate superpower in player
- Count points in end screen
- More levels

## Implementation

First, I was using `Invoke` when an action needed a delay, like when the game is lost or when the player is hit. Later I changed it to use coroutines instead, since it made code simpler. This allows for animations to finish at a different time than the framerate, make the gameplay less abrupt, or avoid the player colliding multiple times with enemies.

`Invoke` is used in Actions. I use Actions instead of custom event delegates, as explained in [this thread](https://answers.unity.com/questions/1739085/when-to-decide-between-using-a-delegate-event-or-a.html).

I use coroutines for the sounds of footsteps of the player, since doing it in `Update` was playing the sound on every frame update, and I need to control the interval at which it plays.

## Architecture

All scripts contain logic only relevant to themselves, and any data flows through the `GameplayManager`, so that it's possible to remove and add scripts without breaking anything. For example, the logic for Weapon is separate from Fireball and from Player. This allows for future different weapons or ammo, without having to change anything in the Player script.

I am using templates to easily introduce different types of game objects with similar behaviors. 

One example are collidable boxes: they all inherit from the same `CollidableController` script for the shared logic (detecting collision with Player, making a noise, etc.), but then each different "collidable" (brick, lootbox) implement `CollectibleController` and have their own different behavior on top. They all need to override the virtual method `OnCollision()`, though.

Another example are collectibles like coins or stars. The `CollidableController` has the logic to detect a trigger collision with Player (the Player doesn't need to be aware of what game objects it can interact), animate and make a sound, and sends calls an Action set in the `GameplayManager` so that there is no hard dependency between the collectible and other game objects. In the case of coins, the game manager updates the coin count and UI; in the case of stars, it changes the Player state to superpower.

I use an enum `GameState` to define game state (won, lost, paused) in a human-readable format.

I use a struct `GameData` to encapsulate all game data. 

## Gameplay

I introduced a new action: fire, which is done with the `Fire1` button mapped to `ctrl`. Fire throws a fireball that kills enemies.

This action is only available when in "superpower" mode, which is enabled by collecting a star. Once hit or dead, the superpower disappears.

Flags are checkpoints. When you die, you start in the latest checkpoint you touched.

You can kill enemies by jumping on top of them or by throwing a fireball.

Lava pits kill you.

The objective is to open the door and touch it.

## Sources

I used [Pixlr Editor](https://pixlr.com/e) to create sprite animation for fireball explosion. 

I used [2D Shooting in Unity tutorial by Brackeys](https://www.youtube.com/watch?v=wkKsl1Mfp5M) for the Fireball and Weapon scripts.

Art for keyboard keys https://gerald-burke.itch.io/geralds-keys.

Pixel font https://emhuo.itch.io/peaberry-pixel-font.

# PAC 2 - Un joc de plataformes

Video: https://youtu.be/mkHRodL8xFE

## Implementation 

I used Cinemachine for the camera to follow the player. I implemented first [the code from the material provided](https://gitlab.com/uoc_vg/prog2d/ed.2021-2/modulo-2/-/blob/master/docs/Soluciones.md#reto-6), but once I did this, I wanted to add a bit more of complexity and Cinemachine was the perfect choice. 

Cinemachine not only tracks the game object you tell it, but also allows to define "damping" so that the camera movement is smoother, and boundary limits to which confine the camera movement. 

The scripts are generic enough to allow for more types of collectibles, enemies and killer colliders (like lava). For example CollectibleController is only used in coin but named collectible so it can be reused in other collectible items.

The lives are dynamic and can be set in the `GameplayManager` component. The UI is generated programmatically with UI Images and sprites.

These are all the scripts: 

- GameplayManager to control the game mechanics, points and score.
- PlayerController to control player movement and interaction with enemies and killers that either remove a life or kill.
- EnemyController to control enemies movement.
- CollectibleController to control collision with player, animate and update score.
- SwitchController to control interaction with the switch which opens the door.
- OpenDoorController to change display of the door and collision with the player.
- LootboxController to detect collision from player from beneath and animations.

## Architecture

I took some inspiration from this [How to architect game code](https://unity.com/how-to/architect-game-code-scriptable-objects) blog post. Although it talks about scriptable objects, which are not implemented, I did follow the recommendation of avoiding creating systems that have hard dependencies (e.g. using Actions) and that are interdependent. GameplayManager script knows about Player and Collectible, but they don't know about GameplayManager.

I used Action instead of SendMessage as a mean of communication between component script, because it allows to attach callbacks and to avoid hard dependencies.

Another architectural decision is detaching presentation from logic. Functions don't modify UI, only data. The `UpdateUI()` function is the single place where UI is modified.

## Gameplay


Use `a` or left arrow to move left, `d` or right arrow to move right. Space bar to jump.

Press `esc` at any time to pause the game.


## Sources

Images by Kenney (Freeware)
https://www.kenney.nl/assets/platformer-pack-redux

Font "Super Mario Bros" by Mario Monsters (Freeware)
https://www.fontspace.com/super-mario-bros-font-f3396

Sound effects from "Free SFX" by Kronbits (https://kronbits.itch.io/freesfx). CCO license (https://creativecommons.org/choose/zero/). 

Some sounds from the original Mario 1, from The Internet Archive
https://archive.org/details/mario_nes_snes_sounds/Mario+1+-+Game+Over.wav
